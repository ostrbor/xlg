# xlg

# Requirements

- network independent: must not be influenced by lost network connection
- simple and reliable: app should not be interrupted by logging errors
- easy to add custom data in the log message
- provide utils for tedious, repetitive tasks (log request, response, func name)
- public link for API clients to inspect error messages
- event storm

# Architecture

Log client: saves logs in local storage.
Collector: collects logs of different services from local storage and sends them to log server.
Log server: receives logs and stores them in central log db.
Notifier: inspects central log db and sends notifications.
Log UI: user interface to logs, public logs.
Mobile APP: interface for notifications.

# Use cases

1. Log about failed function

```go
log.Failed(validate, err).Write()
```

2. Log request/response
```go
log.Request(req).Response(resp).Write()

```

3. Log request/response redacted
```go
log.Req(method, url, reqHeadRedacted, reqBodyRedacted).Resp(code, respHeadRedacted, respBodyRedacted).Write()

```

4. Log summary of job
```go
log.Msg("UPS tracking summary").Ctx(...).Write()
```

5. Context variants
```go
log.Ctx("integer", 1, "string", "", "slice of values", []string{}, "struct", struct{}{})

```

6. Set user from request context
```go
var r *http.Request
u := r.Context().Value("user")
log.User(u)

```

# Log writer

Log file is used for max 5 mins of logging.

Calculate name for current log file: find multiple of 5 that is closest to current minutes.
For calculation use Truncate.
Eg if now 18 mins, then 15 mins is the answer.
Use current date and found minutes value as filename.
If file exists - open for append, else create.
Write log message into file.

# Log collector

Calculate name for previous log file:
 - calc name for current log file;
 - subtract 5 mins from current log file timestamp.

Open log file for reading.
Read data.
Use JSON stream decoder.
Iterate over JSON objects.
Append objects into array.
Send array to log server.

Move file to 'history'.

# Log rotator

Each hour delete logs in 'history' created more than 7 days ago.